const express = require('express')
const app = express()
const port = 3150
//REST
//HTTP Verb - get, post, put, delete, Patch
// respond with "hello Bangladesh" when a GET request is made to the homepage
app.get('/hello', function (req, res) {
  res.send('hello Bangladesh')
})
// app.get('/infix', function (req, res) {
//   res.redirect('http://infixtech.com')
// })
app.get('/', (req, res) => res.send('Welcome'))
app.listen(port, () => console.log(`Blog application listening on port ${port}!`))
